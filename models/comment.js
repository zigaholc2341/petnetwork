const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

//Add Comment Schema
const AddCommentSchema = mongoose.Schema({
    comment:{
        type: String,
        required: true
    },
    forum: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Forum'
    }
});

module.exports = mongoose.model('Comment', AddCommentSchema);

module.exports.getCommentById = function(id, callback){
    Comment.findById(id, callback);
}

module.exports.getCommentByName = function(title, callback){
    const querry = {title: title}
    Comment.findOne(querry, callback);
}

module.exports.addComment = function(newComment, callback){
            newComment.save(callback);
    }