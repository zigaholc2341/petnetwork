const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

//Add CareSearch Schema
const AddCareSearchSchema = mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    surname:{
        type: String,
        required: true
    },
    description:{
        type: String,
        required: true
    },
    datefrom:{
        type: String,
        required: true
    },
    dateto:{
        type: String,
        required: true
    },
    time:{
        type: String,
        required: true
    },
    phone:{
        type:  String,
        required: true
    },
    author:{
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    }
});

const CareSearch = module.exports = mongoose.model('CareSearch', AddCareSearchSchema);

module.exports.getCareSearchById = function(id, callback){
    CareSearch.findById(id, callback);
}

module.exports.getCareSearchByName = function(name, callback){
    const querry = {name: name}
    User.findOne(querry, callback);
}

module.exports.addCareSearch = function(newCareSearch, callback){
            newCareSearch.save(callback);
    }