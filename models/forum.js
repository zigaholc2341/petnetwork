const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

//Add Forum Schema
const AddForumSchema = mongoose.Schema({
    title:{
        type: String,
        required: true,
        unique: true
    },
    question:{
        type: String,
        required: true
    },
    comments: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment'
    }]
});

const Forum = module.exports = mongoose.model('Forum', AddForumSchema);

module.exports.getForumById = function(id, callback){
    Forum.findById(id, callback);
}

module.exports.getForumByName = function(title, callback){
    const querry = {title: title}
    Forum.findOne(querry, callback);
}

module.exports.addForum = function(newForum, callback){
            newForum.save(callback);
    }