const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

//Add Care Schema
const AddCareSchema = mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    surname:{
        type: String,
        required: true
    },
    address:{
        type: String,
        required: true
    },
    city:{
        type: String,
        required: true
    },
    description:{
        type: String,
        required: true
    },
 /*   datefrom:{
        type: String,
        required: true
    },
    dateto:{
        type: String,
        required: true
    },
    time:{
        type: String,
        required: true
    },*/
    price:{
        type: String,
        required: true
    },
    phone:{
        type:  String,
        required: true
    },
    author:{
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    }
});

const Care = module.exports = mongoose.model('Care', AddCareSchema);

module.exports.getCareById = function(id, callback){
    Care.findById(id, callback);
}

module.exports.getCareByName = function(name, callback){
    const querry = {name: name}
    User.findOne(querry, callback);
}

module.exports.addCare = function(newCare, callback){
            newCare.save(callback);
    }