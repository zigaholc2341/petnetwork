export class SearchCare {
  _id: String;
  name: String;
  surname: String;
  description: String;
  datefrom: Date;
  dateto: Date;
  time: String;
  phone: String;
  author: string;
}
