export class Forum{
    _id: string;
    comment: string;
    title: string;
    question: string;
    author: string;
}