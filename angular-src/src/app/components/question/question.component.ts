import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService } from '../../services/auth.service';
import { ValidateService } from 'src/app/services/validate.service';
import { CaresService } from 'src/app/services/cares.service';
import { ForumsService } from 'src/app/services/forums.service';


@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

  title: string;
  question: string

  constructor(
    private router: Router,
    validateService: ValidateService,
    private flashMessage: FlashMessagesService,
    private caresService: CaresService,
    private authService:AuthService,
    private forumsService:ForumsService) { }

  ngOnInit(): void {
  }

  onQuestionSubmit(){
    const forum = {
      title: this.title,
      question: this.question
    }

    this.forumsService.addForum(forum).subscribe(data => {
      if((data as any).success){
        this.flashMessage.show('You added a question', {timeout: 10000});
        this.router.navigate(['/forum']);
      }else{
        this.flashMessage.show('Something went wrong', {timeout: 10000});
        this.router.navigate(['/question']);
      }
    }, err => {
      console.log(err);
      this.flashMessage.show('Something went wrong', {timeout: 10000});
    });
  }

}
