import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Forum } from 'src/app/shared/forumsDB';
import { ForumsService } from 'src/app/services/forums.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  forum:any;
  forums: Forum[];
  comments:string;
  comment: string;

  constructor(
    private forumsService: ForumsService,
    private flashMessage: FlashMessagesService,
    private route: ActivatedRoute,
    private router: Router) { }


  ngOnInit(): void {
    //get id from forum.component.ts
    const id = this.route.snapshot.paramMap.get('id');

    this.forumsService.getForumById(id).subscribe(data => {
      this.forum = data;
      this.comments = this.forum.comments
    })
    }

  onAddCommentSubmit(){

    const id = this.route.snapshot.paramMap.get('id');
    console.log(id);

    const comment = {
      comment: this.comment
    }
    
    console.log("pridem do komentarja: ", comment)

    this.forumsService.addComments(id, comment).subscribe(data => {
      if((data as any).success){
        console.log("success: ", data)
        this.flashMessage.show('Dodali ste komentar', {timeout: 10000});
        //this.router.navigate(['/comment']); -> i need to reload page
        window.location.reload();
      }else{
        console.log("error")
        this.flashMessage.show('Nekaj je narobe!', {timeout: 10000});
        //this.router.navigate(['/question']);
      }
    }, err => {
      console.log(err);
      this.flashMessage.show('Nekaj je narobe!', {timeout: 10000});

    });
  }
}