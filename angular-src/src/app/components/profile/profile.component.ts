import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';
import { CaresService } from 'src/app/services/cares.service';
import { Care } from 'src/app/shared/caresDB';
import { SearchCaresService } from 'src/app/services/search-cares.service';
import { SearchCare } from 'src/app/shared/searchCaresDB';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  user:any;
  care:any;
  cares: Care[];
  selectedCare: Care;

  searchcare: any;
  searchcares: SearchCare[];
  selectedSearchCare: SearchCare;

  constructor(
    public authService:AuthService,
    private userService: UsersService,
    private careService: CaresService,
    private searchcareService: SearchCaresService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.userService.getProfile().subscribe(profile => {
      this.user = (profile as any).user; //profile.user -> spremenil
    },
    err => {
      console.log(err);
      return false;
    });

    this.careService.getMyCare().subscribe((data: any[]) => {
      //console.log("care data date:", data)
      data = data.map((care: any) => {
      //  care.datefrom = this.parseDate(care.datefrom);
      //  care.dateto = this.parseDate(care.dateto);
      return care;
      });
      this.cares = data;
    });

    this.searchcareService.getMySearchCare().subscribe((data: any[]) => {
      //console.log("my searches: ", data)
      data = data.map((searchcare: any) => {
        searchcare.datefrom = this.parseDate(searchcare.datefrom);
        searchcare.dateto = this.parseDate(searchcare.dateto);
      return searchcare;
      });
      this.searchcares = data;
    });
  }
  
  parseDate(dateString: string): Date {
    let date = new Date();
    //console.log("String of dates: ", dateString);
    date.setDate( + dateString.split('.')[0]);  //split ne dela
    date.setMonth( + dateString.split('.')[1]);
    date.setFullYear( + dateString.split('.')[2]);
    return date;
  }

  deleteCare(care, index){
   this.careService.deleteCare(care._id).subscribe((data => {
     console.log("care", care);
     console.log("index", index);
     this.cares.splice(index, 1);
   }))
  }

  deleteSearchCare(searchcare, index){
    this.searchcareService.deleteSearchCare(searchcare._id).subscribe((data => {
      console.log("searchcare", searchcare);
      console.log("index", index);
      this.searchcares.splice(index, 1);
    }));
   }

}
