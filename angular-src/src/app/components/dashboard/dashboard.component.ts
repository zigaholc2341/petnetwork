import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Care } from '../../shared/caresDB';
import { CaresService } from 'src/app/services/cares.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  user:any;
  care:any;
  cares: Care[];
  selectedCare: Care;

  constructor(
    private careService: CaresService,
    private router: Router) { }

// SPREMENI V getCare vse!
  ngOnInit(): void {

    this.careService.getCare().subscribe((data: any[]) => {
      console.log("care data date:", data)
      data = data.map((care: any) => {
       // care.datefrom = this.parseDate(care.datefrom);
       // care.dateto = this.parseDate(care.dateto);
        return care;
      });
      this.cares = data;
    });
  }

 /* parseDate(dateString: string): Date {
    let date = new Date();
    date.setDate( + dateString.split('.')[0]);
    date.setMonth( + dateString.split('.')[1]);
    date.setFullYear( + dateString.split('.')[2]);
    return date;
  }*/
}
