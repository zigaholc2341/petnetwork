import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService } from '../../services/auth.service';
import { ValidateService } from 'src/app/services/validate.service';
import { CaresService } from 'src/app/services/cares.service';
//import { Time } from '@angular/common';

@Component({
  selector: 'app-addcare',
  templateUrl: './addcare.component.html',
  styleUrls: ['./addcare.component.scss']
})
export class AddcareComponent implements OnInit {

  name: String;
  surname: String;
  address: String;
  city: String;
  description: String;
//  datefrom: Date;
//  dateto: Date;
// time: Time;
  price: String;
  phone: String;

  constructor(
    private router: Router,
    validateService: ValidateService,
    private flashMessage: FlashMessagesService,
    private caresService: CaresService,
    private authService:AuthService) { }

  ngOnInit(): void {
  }

  onAddCareSubmit(){
    const care = {
      name: this.name,
      surname: this.surname,
      address: this.address,
      city: this.city,
      description: this.description,
    //  datefrom: this.datefrom,
    //  dateto: this.dateto,
    //  time: this.time,
      price: this.price,
      phone: this.phone
    }

    this.caresService.addCare(care).subscribe(data => {
      if((data as any).success){
        this.flashMessage.show('You added a day care', {timeout: 10000});
        this.router.navigate(['/profile']);
      }else{
        this.flashMessage.show('Something went wrong', {timeout: 10000});
        this.router.navigate(['/addcare']);
      }
    }, err => {
      console.log(err);
      this.flashMessage.show('Something went wrong', {timeout: 10000});
    });

  }

}
