import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddcareComponent } from './addcare.component';

describe('AddcareComponent', () => {
  let component: AddcareComponent;
  let fixture: ComponentFixture<AddcareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddcareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddcareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
