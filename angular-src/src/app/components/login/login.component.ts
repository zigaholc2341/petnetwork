import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ValidateService } from '../../services/validate.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: String;
  password: String;

  constructor(
    private authService:AuthService,
    private router: Router,
    private flashMessage: FlashMessagesService,
    private validateService: ValidateService
  ) { }

  ngOnInit(): void {
  }

onLoginSubmit(){
  const user = {
    username: this.username,
    password: this.password
  }

  //Required fileds
  if (!this.validateService.validateLogin(user)) {
    this.flashMessage.show('Fill in all fields', {timeout: 3000});
    window.location.reload();
    return false;
  }


  //Generate token on login and 
  this.authService.authenticateUser(user).subscribe(data => {
    if((data as any).success = true){
      console.log("auth data", data)
      this.authService.storeUserData((data as any).token, (data as any).user);
      this.flashMessage.show('Vpisani ste', {timeout: 5000});
      this.router.navigate(['dashboard']);
    }else{
      console.log(data);
      this.flashMessage.show((data as any).msg, {timeout: 5000});
      this.router.navigate(['login']);
    }
    
  });

}





}
