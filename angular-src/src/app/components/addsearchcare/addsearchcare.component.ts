import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ValidateService } from 'src/app/services/validate.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { SearchCaresService } from 'src/app/services/search-cares.service';
import { AuthService } from 'src/app/services/auth.service';
import { Time } from '@angular/common';

@Component({
  selector: 'app-addsearchcare',
  templateUrl: './addsearchcare.component.html',
  styleUrls: ['./addsearchcare.component.scss']
})
export class AddsearchcareComponent implements OnInit {

  name: String;
  surname: String;
//  address: String;
  city: String;
  description: String;
  datefrom: Date;
  dateto: Date;
 time: Time;
//  price: String;
  phone: String;

  constructor(
    private router: Router,
    validateService: ValidateService,
    private flashMessage: FlashMessagesService,
    private searchCaresService: SearchCaresService,
    private authService:AuthService) { }

  ngOnInit(): void {
  }

  onAddSearchCareSubmit(){
    const care = {
      name: this.name,
      surname: this.surname,
    //  address: this.address,
      city: this.city,
      description: this.description,
      datefrom: this.datefrom,
      dateto: this.dateto,
      time: this.time,
    //  price: this.price,
      phone: this.phone
    }

    this.searchCaresService.addSearchCare(care).subscribe(data => {
      if((data as any).success){
        this.flashMessage.show('Dodal si iskanje varstva', {timeout: 10000});
        this.router.navigate(['/profile']);
      }else{
        this.flashMessage.show('Nekaj je narobe', {timeout: 10000});
        this.router.navigate(['/addsearchcare']);
      }
    }, err => {
      console.log(err);
      this.flashMessage.show('Nekaj je narobe', {timeout: 10000});
    });

  }
}
