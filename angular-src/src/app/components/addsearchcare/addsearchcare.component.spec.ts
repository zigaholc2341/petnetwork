import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddsearchcareComponent } from './addsearchcare.component';

describe('AddsearchcareComponent', () => {
  let component: AddsearchcareComponent;
  let fixture: ComponentFixture<AddsearchcareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddsearchcareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddsearchcareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
