import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  name: String;
  username: String;
  email: String;
  password: String;

  constructor(private validateService: ValidateService,
    private flashMessage: FlashMessagesService,
    private authService:AuthService,
    private router:Router) { }

  ngOnInit(): void {
  }

  onRegisterSubmit(){
    const user = {
      name: this.name,
      email: this.email,
      username: this.username,
      password: this.password
    }

    //Required fileds
    if (!this.validateService.validateRegister(user)) {
      this.flashMessage.show('Fill in all fields', {timeout: 5000});
      return false;
    }

    //Validate email
    if (!this.validateService.validateEmail(user.email)) {
      this.flashMessage.show('Email is invalid', {timeout: 5000});
      return false;
    }

    //Register user
    this.authService.registerUser(user).subscribe(data => {
      if((data as any).success){
        this.flashMessage.show('You are now registered and can log in', {timeout: 5000});
        this.router.navigate(['/login']);
      }else{
        this.flashMessage.show('Your email or username may alredy be in use!', {timeout: 5000});
        this.router.navigate(['/register']);
      }
    });
  }

}
