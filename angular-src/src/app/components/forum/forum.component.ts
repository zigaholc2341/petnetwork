import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Forum } from 'src/app/shared/forumsDB';
import { ForumsService } from 'src/app/services/forums.service';



@Component({
  selector: 'app-forum',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.scss']
})
export class ForumComponent implements OnInit {

  forum:any;
  forums: Forum[];
  selectedForum: Forum;
  sharedId: any;

  constructor(
    private forumsService: ForumsService,
    private router: Router) { }

  ngOnInit(): void {
    this.forumsService.getForum().subscribe((data: any[]) => {
      console.log("forums:", data)
      data = data.map((forum: any) => {
        return forum;

      })
      this.forums = data;
    })
  }
  

  //gets forums ID -> it needs to pass it to comment.component.ts
  getId(forum, index){
    this.forumsService.getForumById(forum._id).subscribe((data => {
    //console.log(forum._id)
    this.sharedId = forum._id;
    return forum._id;
    }));
    //console.log(forum._id);
    this.router.navigate(['/comment', forum._id]);
  }

  
  

}
