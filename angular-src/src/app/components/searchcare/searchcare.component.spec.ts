import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchcareComponent } from './searchcare.component';

describe('SearchcareComponent', () => {
  let component: SearchcareComponent;
  let fixture: ComponentFixture<SearchcareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchcareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchcareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
