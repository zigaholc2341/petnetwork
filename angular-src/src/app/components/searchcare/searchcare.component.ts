import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Care } from '../../shared/caresDB';
import { SearchCaresService } from 'src/app/services/search-cares.service';

@Component({
  selector: 'app-searchcare',
  templateUrl: './searchcare.component.html',
  styleUrls: ['./searchcare.component.scss']
})
export class SearchcareComponent implements OnInit {

  user:any;
  care:any;
  cares: Care[];
  selectedCare: Care;

  constructor(
    private searchCaresService: SearchCaresService,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.searchCaresService.getSearchCare().subscribe((data: any[]) => {
      console.log("care data date:", data)
      data = data.map((care: any) => {
        care.datefrom = this.parseDate(care.datefrom);
        care.dateto = this.parseDate(care.dateto);
        return care;
      });
      this.cares = data;
    });
  }

  parseDate(dateString: string): Date {
    let date = new Date();
    date.setDate( + dateString.split('.')[0]);  //split ne dela
    date.setMonth( + dateString.split('.')[1]);
    date.setFullYear( + dateString.split('.')[2]);
    return date;
  }

}
