import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';

import { RouterModule, Routes } from '@angular/router';

import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProfileComponent } from './components/profile/profile.component';
import { HttpClientModule } from '@angular/common/http';

import { ValidateService } from './services/validate.service';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './guards/auth.guard';
import { JwtModuleOptions, JwtModule } from '@auth0/angular-jwt';
import { AddcareComponent } from './components/addcare/addcare.component';
import { FooterComponent } from './components/footer/footer.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { ForumComponent } from './components/forum/forum.component';
import { QuestionComponent } from './components/question/question.component';
import { CommentComponent } from './components/comment/comment.component';
import { SearchcareComponent } from './components/searchcare/searchcare.component';
import { AddsearchcareComponent } from './components/addsearchcare/addsearchcare.component';
library.add(fas);

const appRoutes: Routes = [
  {path:'', component: HomeComponent},
  {path:'register', component: RegisterComponent},
  {path:'login', component: LoginComponent},
  {path:'dashboard', component: DashboardComponent, canActivate:[AuthGuard]},
  {path:'profile', component: ProfileComponent, canActivate:[AuthGuard]},
  {path:'addcare', component: AddcareComponent, canActivate:[AuthGuard]},
  {path:'forum', component: ForumComponent, canActivate:[AuthGuard]},
  {path:'question', component: QuestionComponent, canActivate:[AuthGuard]},
  {path:'comment/:id', component: CommentComponent, canActivate:[AuthGuard]},
  {path:'searchcare', component: SearchcareComponent, canActivate:[AuthGuard]},
  {path:'addsearchcare', component: AddsearchcareComponent, canActivate:[AuthGuard]},
]

const JWT_Module_Options: JwtModuleOptions = {
  config: {
  }
};

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    DashboardComponent,
    ProfileComponent,
    AddcareComponent,
    FooterComponent,
    ForumComponent,
    QuestionComponent,
    CommentComponent,
    SearchcareComponent,
    AddsearchcareComponent
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    FlashMessagesModule.forRoot(),
    JwtModule.forRoot(JWT_Module_Options),
    FontAwesomeModule
  ],
  providers: [ValidateService, AuthService, AuthGuard],
  bootstrap: [AppComponent]
})

export class AppModule { }
