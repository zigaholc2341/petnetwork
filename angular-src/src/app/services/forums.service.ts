import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';
import { Forum } from '../shared/forumsDB';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ForumsService {

  constructor(
    private auth: AuthService,
    private http: HttpService,
    ) { }

  

  addForum(forum){
    return this.http.post(environment.url + '/forums/', forum);
  }

  getForum(): Observable<Forum[]> {
    return this.http.get<Forum[]>(environment.url + '/forums/all');
  }


  addComments(id, comment) {
    return this.http.post(environment.url + '/forums/comment/' + id, comment);
  }

   getForumById(id) {
     return this.http.get<Forum>(environment.url + '/forums/comments/' + id);
   }
}
