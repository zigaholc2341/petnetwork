import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { HttpService } from './http.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private auth: AuthService,
    private http: HttpService
  ) { }

  getProfile(){
    this.auth.loadToken();
    return this.http.get(environment.url + '/users/profile');
  }

}
