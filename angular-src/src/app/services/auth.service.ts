import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {JwtHelperService} from '@auth0/angular-jwt';
import 'rxjs/add/operator/map';
import { HttpService } from './http.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authToken:any;
  user:any;
  care:any;

  constructor(
    private http:HttpClient,
    private jwtHelper: JwtHelperService,  
  ) { }
 
  getJWTtoken(){
   return localStorage.getItem('token'); 
  }

  //USERS
  registerUser(user){
    return this.http.post(environment.url + '/users/register', user); 
  }

  authenticateUser(user){
    return this.http.post(environment.url + '/users/authenticate', user); 
  }

  storeUserData(token, user){
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  loadToken(){
    const token = localStorage.getItem('token');
    this.authToken = token;
  }


  loggedIn(){
    if (localStorage.getItem('token')) {
      return !this.jwtHelper.isTokenExpired(localStorage.getItem('token'));      
    }
  
  }

  logout(){
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }
}