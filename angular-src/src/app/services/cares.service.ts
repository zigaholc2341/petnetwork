import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';
import { Care } from '../shared/caresDB';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CaresService {
  
  constructor(
    private auth: AuthService,
    private http: HttpService,
  ) { }

  //CARES
  addCare(care){
    return this.http.post(environment.url + '/cares/', care);
  }

  getCare(): Observable<Care[]> {
    return this.http.get<Care[]>(environment.url +  '/cares/all');
  }

  getMyCare(): Observable<Care[]> {
    return this.http.get<Care[]>(environment.url + '/cares/my');
  }

  deleteCare(id){
    return this.http.delete<Care[]>(environment.url + '/cares/my/' + id);
  }

}
