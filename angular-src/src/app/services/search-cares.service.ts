import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';
import { SearchCare } from '../shared/searchCaresDB';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SearchCaresService {

  constructor(
    private auth: AuthService,
    private http: HttpService,
  ) { }

  //SEARCH CARES
  addSearchCare(searchCare){
    return this.http.post(environment.url + '/careSearchs/', searchCare);
  }

  getSearchCare(): Observable<SearchCare[]> {
    return this.http.get<SearchCare[]>(environment.url + '/careSearchs/all');
  }

  getMySearchCare(): Observable<SearchCare[]> {
    return this.http.get<SearchCare[]>(environment.url + '/careSearchs/my');
  }

  deleteSearchCare(id){
    return this.http.delete<SearchCare[]>(environment.url + '/careSearchs/my/' + id);
  }

}
