# petNetwork 🐶

# About Project

<p>
“Kako poskrbeti za domačo žival, ko nas (lastnikov) ni doma?” Takšno vprašanje se nam poraja po glavi prav pogosto in predstavlja večji problem, kot si predstavljamo. Lastniki domačih živali se zato nagibajo k različnim rešitvam. Nekateri se obrnejo na sorodnike, prijatelje in znance, na koncu pa celo na kakšen hotel za živali, ki pa je praviloma drag. Domača žival je lahko sama doma nekaj ur, ne pa več dni, ko gremo, na primer, na službeno potovanje in ne moremo svojega ljubljenčka vzeti zraven. Nekateri svoje ljubljenčke tudi pustijo doma prepuščene same sebi, kar pa lahko prinese dodatne težave, saj lahko živali povzročijo veliko škodo.
</p>