const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');

const User = require('../models/user');
const CareSearch = require('../models/careSearch');

// AddCareSearch
router.post('/', passport.authenticate('jwt', {session:false}),  (req, res, next) => {
    let user = JSON.parse(JSON.stringify(req.user));
    let newCare = new CareSearch({
        name: req.body.name,
        surname: req.body.surname,
        description: req.body.description,
        datefrom: req.body.datefrom,
        dateto: req.body.dateto,
        time: req.body.time,
        phone:req.body.phone,
        author: user._id
    });

    CareSearch.addCareSearch(newCare, (err, care) => {
        if(err){
            console.log(err)
            res.json({success: false, msg: 'Failed to publish search for care'});
        } else {
            res.json({success: true, msg: 'Published search for care'});
        }
    });
});

//get Care on dashboard
router.get('/all', function(req, res) {
    console.log('Get request for all search cares');
    CareSearch.find({}).exec(function(err, cares){
        if(err){
            console.log("Error getting search cares");
        }else{
            res.json(cares);
        }
    });
});

//delete cares
router.delete('/my/:id', passport.authenticate('jwt', {session:false}), function(req, res){
    console.log('Deleting published search care');
    CareSearch.findByIdAndRemove(req.params.id, function(err, deleteCare){
        if (err) {
            res.send("Error deleting search care");
        }else{
            res.json(deleteCare);
        }
    });
});

//Get just my cares
router.get('/my', passport.authenticate('jwt', {session:false}), function(req, res) {
    console.log(typeof req.user);
    console.log(req.user);
    CareSearch.find({'author': req.user._id}, (err, cares) => {
       if(err) {
         console.log(err);
         res.status(500);
         res.send(err)
       } else {
         //res.json(200, {currentUser: req.user, cares: cares});
         res.json(cares)
       }
    });
});

module.exports = router;