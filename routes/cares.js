const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');

const User = require('../models/user');
const Care = require('../models/care');

// AddCare
router.post('/', passport.authenticate('jwt', {session:false}),  (req, res, next) => {
    let user = JSON.parse(JSON.stringify(req.user));
    let newCare = new Care({
        name: req.body.name,
        surname: req.body.surname,
        address: req.body.address,
        city: req.body.city,
        description: req.body.description,
       // datefrom: req.body.datefrom,
       // dateto: req.body.dateto,
       // time: req.body.time,
        price: req.body.price,
        phone:req.body.phone,
        author: user._id

    });
    Care.addCare(newCare, (err, care) => {
        if(err){
            console.log(err)
            res.json({success: false, msg: 'Failed to publish care'});
        } else {
            res.json({success: true, msg: 'Published care'});
        }
    });
});

//get Care on dashboard
router.get('/all', function(req, res) {
    console.log('Get request for all cares');
    Care.find({}).exec(function(err, cares){
        if(err){
            console.log("Error getting cares");
        }else{
            res.json(cares);
        }
    });
});

//delete cares
router.delete('/my/:id', passport.authenticate('jwt', {session:false}), function(req, res){
    console.log('Deleting published care');
    Care.findByIdAndRemove(req.params.id, function(err, deleteCare){
        if (err) {
            res.send("Error deleting care");
        }else{
            res.json(deleteCare);
        }
    });
});

//Get just my cares
router.get('/my', passport.authenticate('jwt', {session:false}), function(req, res) {
    console.log(typeof req.user);
    console.log(req.user);
    Care.find({'author': req.user._id}, (err, cares) => {
       if(err) {
         console.log(err);
         res.status(500);
         res.send(err)
       } else {
         //res.json(200, {currentUser: req.user, cares: cares});
         res.json(cares)
       }
    });
});

module.exports = router;