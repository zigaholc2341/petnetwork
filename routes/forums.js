const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');

const Forum = require('../models/forum');
const Comment = require('../models/comment');

// AddForum
router.post('/', (req, res, next) => {
    let newForum = new Forum({
        title: req.body.title,
        question: req.body.question,
        comment: req.body.comment
    });
    
    Forum.addForum(newForum, (err, forum) => {
        if(err){
            console.log(err)
            res.json({success: false, msg: 'Failed to publish forum question'});
        } else {
            res.json({forum, success: true, msg: 'Published question of forum'});
        }
    });
});

//get Forum
router.get('/all', function(req, res) {
    console.log('Get request for all forums');
    Forum.find({})
    .populate(
        'comment')
    .exec(function(err, forums){
        if(err){
            res.json({success: false, msg: 'failed getting all', forums})
        }else{
            res.json(forums);
        }
    });
});

//create a comment for a question
router.post('/comment/:forumId', function(req, res) {
    const comment = new Comment(req.body);
      comment.save().then(comment => {
        return Forum.findById(req.params.forumId);
      })
      .then(forum => {
        forum.comments.unshift(comment);
        return forum.save();
      })
      .then(forum => {
        res.json({success: true, msg: 'comment posted', forum});
      })
      .catch(err => {
        res.json(comment)
      });
  });

//get forum and it's comments
router.get('/comments/:Id', function(req, res, next) {
    console.log('Get request for comments');
    Forum.findById(req.params.Id)
    .populate('comments')
    .exec(function(err, forum){
        if(err){
            console.log("Error getting forums");
        }else{
            res.json(forum);
        }
    });
});

module.exports = router;
